#! /usr/bin/python3
import hiyapyco
import argparse

parser = argparse.ArgumentParser(description='Merge multi document YAML files. (Made for Kubernetes configuration)')
parser.add_argument('multidoc_file', metavar='MULTIDOC_YAML', type=str, nargs=1,
                    help='The fultidoc YAML file to merge into')
parser.add_argument('merge_file', metavar='YAML', type=str, nargs=1,
                    help='The YAML file to merge')
parser.add_argument('--add-to-pod-template', '-t', action='store_const', const=True, default=False,
                    help="Add the YAML file to Deployment spec.template as well (Only modifies Services and Deployments)")
parser.add_argument('--kind', '-k', metavar='STRING', type=str, nargs=1, default=None,
                    help="Comma seperated list of Resources to merge into (Matches onto the 'kind' key in the doc)")

args = parser.parse_args()
multidoc_file = args.multidoc_file[0]
merge_file = args.merge_file[0]
if args.kind and args.kind[0]:
    kinds = list(map(str.strip, args.kind[0].split(',')))
else:
    kinds = None

with open(multidoc_file, "r") as f:
    merged_files = list()
    yaml_file = f.read()
    for yaml_conf in yaml_file.split("---"):
        yaml_conf = yaml_conf.strip("\n")
        yaml_dict = hiyapyco.load(yaml_conf)
        if not kinds or "kind" in yaml_dict and yaml_dict["kind"] in kinds:
            merged_file = hiyapyco.load(yaml_conf ,merge_file, method=hiyapyco.METHOD_MERGE, interpolate=True)
            if args.add_to_pod_template and yaml_dict["kind"] == "Deployment":
                pass
                merged_file['spec']['template'] = hiyapyco.load(hiyapyco.dump(merged_file['spec']['template'], default_flow_style=False) ,merge_file, method=hiyapyco.METHOD_MERGE, interpolate=True)
            merged_files.append(hiyapyco.dump(merged_file, default_flow_style=False))
        else:
            merged_files.append(yaml_conf + "\n")
    print("---\n".join(merged_files), end="")

