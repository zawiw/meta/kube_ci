FROM alpine:3

ARG HELM_VERSION
ARG KUBECTL_VERSION

ENV TAR_FILE="helm-${HELM_VERSION}-linux-amd64.tar.gz"

WORKDIR /root

RUN apk --update --no-cache add jq python3 py3-pip bash curl ca-certificates git gettext
RUN pip3 install hiyapyco
RUN rm -f /var/cache/apk/*

RUN curl -o helm.tar.gz https://get.helm.sh/helm-${HELM_VERSION}-linux-amd64.tar.gz && \
    tar -zxvf helm.tar.gz && \
    install linux-amd64/helm /usr/local/bin/helm && \
    rm -rf linux-amd64 && \
    rm helm.tar.gz


RUN curl -o kubectl https://storage.googleapis.com/kubernetes-release/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl
RUN install kubectl /usr/local/bin/ && rm kubectl

RUN mkdir -p /usr/local/bin
COPY scripts/* /usr/local/bin/

SHELL ["/bin/bash", "-c"]

ENTRYPOINT ["/bin/bash", "-l", "-c"]
